package com.example.photonassessment

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavArgumentBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.*
import androidx.navigation.navArgument

enum class Screen {
    HOME,
    DETAIL,
}
sealed class NavigationItem(val route: String) {
    object Home : NavigationItem(Screen.HOME.name)
    object Detail : NavigationItem(Screen.DETAIL.name)
}

@Composable
fun Navigation(schoolViewModel: SchoolViewModel){
    val navController = rememberNavController()

    NavHost(navController = navController, startDestination = NavigationItem.Home.route) {
        composable(NavigationItem.Home.route) {
            // Define your home screen composable here
            MainScreen(schoolViewModel = schoolViewModel, navController = navController)
        }
        composable(
            NavigationItem.Detail.route + "?dbn={dbn}&schoolname={schoolName}&desc={desc}",
                arguments = listOf(
                    navArgument(name = "dbn"){
                        type = NavType.StringType
                        nullable = true},
                    navArgument(name = "schoolname"){
                        type = NavType.StringType
                        nullable = true},
                    navArgument(name = "desc"){
                        type = NavType.StringType
                        nullable = true},
                )
        ) {
            // Define your details screen composable here
            val schoolDetail = SchoolDetail(
                dbn = it.arguments?.getString("dbn") ?: "",
                schoolName = it.arguments?.getString("schoolname") ?: "",
                overview = it.arguments?.getString("desc") ?: ""
            )
            DetailScreen(schoolDetail = schoolDetail)
        }
    }
}

