package com.example.photonassessment

import android.util.Log
import androidx.lifecycle.LiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class SchoolRepository() {

    private val schoolInfoService: SchoolInfoService = SchoolInfoService()

    suspend fun fetchSchoolList(): List<SchoolDetail> = withContext(Dispatchers.IO) {
            Log.d("Debug", "fetchSchoolList called Thread : ${Thread.currentThread().id}")
            var schools = listOf<SchoolDetail>()
            val schoolAPIResponse = schoolInfoService.schoolAPI.getSchoolList().execute()
            if(schoolAPIResponse.isSuccessful){
                val schoolList: List<SchoolDetail>? = schoolAPIResponse.body()
                Log.d("Debug", "Successful Response, ${schoolList?.size}")
                if(schoolList != null) schools = schoolList
            }else{
                Log.d("Debug", "error response : ${schoolAPIResponse.errorBody().toString()}")
            }
        schools
    }
}