package com.example.photonassessment

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MainScreen(
    title: String = "NY School List",
    schoolViewModel: SchoolViewModel,
    navController: NavHostController
    ){

    val schoolList by schoolViewModel.schoolList.observeAsState(listOf<SchoolDetail>())

    LaunchedEffect(key1 = Unit) {
        schoolViewModel.getSchoolList()
    }

    Scaffold(
        topBar = {
            TopAppBar(
                modifier = Modifier.background(MaterialTheme.colorScheme.primary),
                title = { Text(title) }
            )
        }
    ){
        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            verticalArrangement = Arrangement.SpaceEvenly
        ){
            items(schoolList){
                SchoolItem(schoolDetail = it, navController = navController)
            }
        }
    }
}

@Composable
fun SchoolItem(
    schoolDetail: SchoolDetail,
    navController: NavHostController){

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .clickable {
                navController.navigate(NavigationItem.Detail.route +
                    "?dbn=${schoolDetail.dbn}&" +
                        "schoolname=${schoolDetail.schoolName}&" +
                        "desc=${schoolDetail.overview}")
            }
            .border(border = BorderStroke(1.dp, Color.White))
            .padding(16.dp)
            .wrapContentHeight()
    ){
        Text(
            text = schoolDetail.schoolName,
            fontSize = 18.sp,
            fontWeight = FontWeight.Normal
        )
        Text(
            text = schoolDetail.dbn,
            fontSize = 14.sp
        )
    }
}