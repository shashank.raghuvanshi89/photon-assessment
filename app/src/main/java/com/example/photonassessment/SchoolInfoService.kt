package com.example.photonassessment

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import retrofit2.http.GET

interface SchoolAPI {
    @GET("s3k6-pzi2.json")
    fun getSchoolList(): Call<List<SchoolDetail>>
}

object RetrofitClient {

    private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

    fun getClient(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(JacksonConverterFactory.create())
            .build()
}

class SchoolInfoService{
    val retrofit = RetrofitClient.getClient()
    val schoolAPI = retrofit.create(SchoolAPI::class.java)
}