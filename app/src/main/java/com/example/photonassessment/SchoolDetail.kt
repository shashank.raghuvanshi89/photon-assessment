package com.example.photonassessment

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class SchoolDetail (
    @JsonProperty("dbn") val dbn: String,
    @JsonProperty("school_name") val schoolName: String,
    @JsonProperty("overview_paragraph") val overview: String
)
