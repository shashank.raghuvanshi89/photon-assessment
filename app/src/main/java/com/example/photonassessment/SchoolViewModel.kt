package com.example.photonassessment

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class SchoolViewModel: ViewModel() {
    private val repository: SchoolRepository = SchoolRepository()

    private val _schoolList = MutableLiveData<List<SchoolDetail>>()
    val schoolList: LiveData<List<SchoolDetail>>
        get() = _schoolList

    fun getSchoolList() {
        Log.d("Debug", "GetSchoolList called Thread : ${Thread.currentThread().id}")
        viewModelScope.launch {
            val _schoolDetails = repository.fetchSchoolList()
            withContext(Dispatchers.Main){
                _schoolList.value = _schoolDetails
            }
        }
    }
}