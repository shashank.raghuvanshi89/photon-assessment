package com.example.photonassessment

import android.util.Log
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun DetailScreen(schoolDetail: SchoolDetail){
    Log.d("Debug", "school name : ${schoolDetail.schoolName}")
    Column (
        modifier = Modifier
            .fillMaxSize()
            .padding(8.dp),
        horizontalAlignment = Alignment.Start,
        verticalArrangement = Arrangement.Top
    ){
        Text(
            modifier = Modifier.padding(8.dp),
            text = schoolDetail.schoolName,
            fontSize = 22.sp,
            fontWeight = FontWeight.Bold
        )
        Spacer(
            modifier = Modifier.height(24.dp)
        )
        Text(
            modifier = Modifier.padding(16.dp),
            text = schoolDetail.overview,
            fontSize = 16.sp,
            fontWeight = FontWeight.Normal
        )
    }
}